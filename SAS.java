import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.SortedMap;
import java.util.TreeMap;
import java.lang.NumberFormatException;
import java.io.PrintStream;

public class SAS {

    // A list of labels and their associated addresses
    private static Map<String, Integer> label_list;

    // A list of labels that have not yet been matched to an address
    // i.e. still waiting on the instruction
    private static List<String> unattached_labels;

    // current address of the parser
    private static int address;

    private static final List<String> command_options = Arrays.asList(
        "HLT", "LDA", "LDI", "STA", "STI", "ADD", "SUB", "JMP", "JPZ", "DAT");

    // Any instructions that had labels instead of address listed
    // Note: because there is no "pair" or "tuple" class in the defualt java
    // libararies, a map with one key:value was used instead
    private static SortedMap<Integer, Map<String, String>> unmatched_code;
    // Any instructions that had numberical address defined
    private static SortedMap<Integer, Map<String, Integer>> matched_code;

    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        label_list = new HashMap<String, Integer>();
        address = 0;
        unattached_labels = new ArrayList<String>();
        unmatched_code = new TreeMap<>();
        matched_code = new TreeMap<>();
        while (scan.hasNextLine()) {
            if (!trim_line(scan.nextLine())) {
                return;
            }
        }
        if (address >= 16) {
            System.err.println("Error: RAM Limit exceeded. Max instruction limit: 16");
            return;
        }
        if (unattached_labels.size() != 0) {
            System.err.println("Error: no trailing/unattached labels");
            return;
        }
        if (!complete_code_lines()) {
            return;
        }
        output_bytes(System.out);
    }

    // This helper method essenitaly gets rid of whitespace on a line
    // The line is then split into each non-whitespace element and passed to the
    // process_line method. This method also gets rid of comments from the line
    //
    // returns a boolean based on whether the line was successfully parsed
    private static boolean trim_line(String input) {
        String[] line = input.trim().split("\\s+");
        if (!line[0].isEmpty()) {
            int elements = 0;
            while (elements < line.length && !line[elements].equals("#")) {
                elements++;
            }
            if (elements > 0) {
                String[] no_comments = new String[elements];
                for (int i = 0; i < elements; i++) {
                    no_comments[i] = line[i];
                }
                return process_line(no_comments);
            }
        }
        return true;
    }

    //writing the instructions to standard output
    private static void output_bytes(PrintStream output) {
        for (Integer line: matched_code.keySet()) {
            for (String command : matched_code.get(line).keySet()) {
                output.write(command_byte(command) + matched_code.get(line).get(command));
                output.flush();
            }
        }
    }

    //simple mehtod to return the value associated with each command
    private static int command_byte(String command) {
        switch (command) {
            case "HLT":
                return 0;
            case "DAT":
                return 0;
            case "LDA":
                return 16;
            case "LDI":
                return 32;
            case "STA":
                return 48;
            case "STI":
                return 64;
            case "ADD":
                return 80;
            case "SUB":
                return 96;
            case "JMP":
                return 112;
            case "JPZ":
                return 128;
            default:
                return 0;
        }
    }

    // For any instructions that listed a label instead of a numerical address,
    // this method will replace the label with the associated address and add it to
    // matched_code
    //
    // returns a boolean on whether all labels could be succesfully matched
    private static boolean complete_code_lines() {
        for (Integer line: unmatched_code.keySet()) {
            Map<String, String> temp = unmatched_code.get(line);
            for (String command: temp.keySet()) {
                String label = temp.get(command);
                if (!label_list.containsKey(label)) {
                    System.err.println("Error: label not defined: " + label);
                    return false;
                }
                Map<String, Integer> match = new HashMap<>();
                match.put(command, label_list.get(label));
                matched_code.put(line, match);
            }
        }
        unmatched_code.clear();
        return true;
    }


    // The main method for processing a line
    private static boolean process_line(String line[]) {
        boolean hasLabel = false;
        for (int i = 0; i < line.length; i++) {
            if (line[i].charAt(line[i].length() - 1) == ':') {
                // This section deals with any input that could be a label
                if (line[i].length() > 1) {
                    if (!hasLabel) {
                        hasLabel = true;
                    } else {
                        System.err.println("Error: cannot have multiple labels per line");
                        return false;
                    }
                    try {
                        int l = Integer.parseInt(line[i].substring(0, line[i].length() - 1));
                        System.err.println("Error: label name cannot be a number");
                    } catch (NumberFormatException e) { }
                    unattached_labels.add(line[i].substring(0, line[i].length() -1));
                } else {
                    System.err.println("Error: colon with no label name beforehand");
                    return false;
                }
            } else {
                // This section deals with any input that is not a label
                if (!command_options.contains(line[i])) {
                    System.err.println("Error: invalid operation option: " + line[i]);
                    return false;
                }
                if (line[i].equals("HLT") && i == line.length - 1) {
                    // This case is if there is a HLT instruction and no address afterwards, which is allowed
                    Map<String, Integer> pair = new HashMap<>();
                    pair.put(line[i], 0);
                    matched_code.put(address, pair);
                } else {
                    if (i + 1 != line.length - 1) {
                        // If line element is an insutcion, then there can only be one thing after it, the adress,
                        // aside from comments which have already been removed
                        System.err.println("Error: too few/many fields operation");
                        return false;
                    }
                    try {
                        int temp = Integer.parseInt(line[i+1]);
                        // If the parseInt method does not throws a NumberFormatException, the address is numerical
                        if (line[i].equals("DAT")) {
                            if (temp >= 256 || temp < 0) {
                                System.err.println("Error: Data value greater than 255 or negative. Value: " + temp);
                                return false;
                            }
                        } else {
                            if (temp >= 16 || temp < 0) {
                                System.err.println("Error: Operation address larger than 15 or negative. Value: " + temp);
                                return false;
                            }
                        }
                        Map<String, Integer> pair = new HashMap<>();
                        pair.put(line[i], temp);
                        matched_code.put(address, pair);
                    } catch (NumberFormatException e) {
                        // If the parseInt method throws a NumberFormatException, then the address is a label
                        Map<String, String> pair = new HashMap<>();
                        pair.put(line[i], line[i+1]);
                        unmatched_code.put(address, pair);
                    }
                }
                // If the parser was successfully apple to parse an instruction, then all previous labels listed
                // are now associated with this instruction
                for (int j = 0; j < unattached_labels.size(); j++) {
                    if (label_list.containsKey(unattached_labels.get(j))) {
                        System.err.println("Error: duplicate label: " + unattached_labels.get(j));
                        return false;
                    } else {
                        label_list.put(unattached_labels.get(j), address);
                    }
                }
                unattached_labels.clear();
                // If the instruction was succesffuly parsed, then there either wasn't an
                // element afterward or the next element was an address, so we increment i
                i++;
                address++;
            }
        }
        return true;
    }
}
